async function fetchData(url) {
  // <-- start
  // TODO 24: 通过await/async实现异步请求
  try {
    const response = await fetch(url);
    return response.json();
  } catch (error) {
    throw error;
  }
  // end -->
}

const URL = 'http://localhost:3000/api';
fetchData(URL).then(result => document.writeln(result.name));
