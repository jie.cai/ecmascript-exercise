function chooseMultiplesOfThree(collection) {
  // TODO 1: 在这里写实现代码
  return collection.filter(num => num % 3 === 0);
}

function chooseNoRepeatNumber(collection) {
  // TODO 2: 在这里写实现代码
  return Object.keys(
    collection.reduce((collect, num) => {
      const temp = collect;
      temp[`${num}`] = true;
      return temp;
    }, {})
  ).map(i => +i);
}

export { chooseMultiplesOfThree, chooseNoRepeatNumber };
