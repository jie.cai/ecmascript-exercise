// TODO 14: 在这里写实现代码
/* eslint class-methods-use-this: ["error", { "exceptMethods": ["introduce"] }] */
import Person from './person';

export default class Worker extends Person {
  constructor(...args) {
    super(...args);
  }

  introduce() {
    return 'I am a Worker. I have a job.';
  }
}
